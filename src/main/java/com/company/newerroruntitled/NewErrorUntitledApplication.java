package com.company.newerroruntitled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class NewErrorUntitledApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewErrorUntitledApplication.class, args);
    }
}
