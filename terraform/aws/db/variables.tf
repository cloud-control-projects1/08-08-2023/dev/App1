variable "name" {
  type = string
}

variable "vpc_id" {}

variable "instance_class" {
  type = string
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "storage" {
  type = number
}

variable "user" {
  type = string
}

variable "password" {
  type    = string
  default = null
}

variable "random_password" {
  type    = bool
  default = true
}

variable "multi_az" {
  type    = bool
  default = false
}

variable "performance_insights_enabled" {
  type    = bool
  default = false
}

variable "performance_insights_retention_period" {
  type    = number
  default = 7
}

variable "subnet_group_name" {
  type = string
}

variable "enabled_cloudwatch_logs_exports" {
  type    = list(string)
  default = []
}

variable "monitoring_interval" {
  type    = number
  default = 0
}

variable "enhanced_monitoring_arn" {
  type    = string
  default = null
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "source_security_group_id" {
}
