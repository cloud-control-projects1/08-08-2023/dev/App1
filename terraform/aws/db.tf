module "new-error-untitled_db" {
  source = "./db"

  name                                  = var.new-error-untitled_db_name
  engine                                = var.new-error-untitled_db_engine
  engine_version                        = var.new-error-untitled_db_engine_version
  instance_class                        = var.new-error-untitled_db_instance_class
  storage                               = var.new-error-untitled_db_storage
  user                                  = var.new-error-untitled_db_user
  password                              = var.new-error-untitled_db_password
  random_password                       = var.new-error-untitled_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.new-error-untitled_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
